provider "aws" { 
  access_key = "${var.aws_access_key}" 
  secret_key = "${var.aws_secret_key}" 
  region = "${var.region}" 
  }
resource "aws_instance" "instance" { 
  ami = "${var.ami}" 
  instance_type = "${var.instance_type}" 
  key_name = "${var.key_name}" 
  vpc_security_group_ids = ["${var.vpc_security_group_ids}"] 
  subnet_id = "${var.subnet_id}"
   associate_public_ip_address = "${var.associate_public_ip_address}" 
   count = "${var.instance_count}" 
   root_block_device { 
    volume_type = "${var.volume_type}" 
    volume_size = "${var.root_volume_size}" 
    delete_on_termination = "${var.delete_on_termination}"