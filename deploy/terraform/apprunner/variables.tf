variable "aws_access_key" { 
  default = "your_aws_access_key" 
  }
  variable "aws_secret_key" { 
    default = "your_aws_secret_key" }
    variable "region" { default = "ap-southeast-1" }
    variable "availability_zone" { 
      default = "ap-southeast-1a" }
      variable "ami" { default = "ami-05b891753d41ff88f" }
      variable "instance_type" { default = "t2.micro" }
      variable "root_volume_size" { default = 22}